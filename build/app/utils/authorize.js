"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExcludedPath = exports.checkRestaurantOwner = exports.validateRole = exports.authorize = void 0;
const jsonwebtoken_1 = require("jsonwebtoken");
const restaurant_services_1 = __importDefault(require("../feature-module/restaurant/restaurant.services"));
const authorize = (excludedPaths) => {
    return (req, res, next) => {
        var _a;
        try {
            // if(excludedPaths.find(e=>e.path===req.url && e.methods===req.method)){return next()}
            const token = (_a = req.headers.authorization) === null || _a === void 0 ? void 0 : _a.split(" ")[1];
            if (!token)
                return next({ message: "UNAUTHORIZED", statusCode: 401 });
            const { JWT_SECRET } = process.env;
            const result = (0, jsonwebtoken_1.verify)(token, JWT_SECRET || "");
            console.log(result);
            res.locals.tokenInfo = result;
            next();
        }
        catch (e) {
            next({ message: "UNAUTHORIZED", statusCode: 401 });
        }
    };
};
exports.authorize = authorize;
const validateRole = (roles) => {
    return (req, res, next) => {
        try {
            const { role } = res.locals.tokenInfo;
            if (roles.includes(role)) {
                next();
            }
            else {
                next({ message: "UNAUTHORIZED", statusCode: 401 });
            }
        }
        catch (e) {
            next({ message: "Something went wrong", statusCode: 401 });
        }
    };
};
exports.validateRole = validateRole;
const checkRestaurantOwner = () => {
    return (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { id } = res.locals.tokenInfo;
            const restaurantId = req.params.id;
            console.log(restaurantId);
            const restaurant = yield restaurant_services_1.default.findOne({ _id: restaurantId });
            if (!restaurant) {
                return res.status(404).json({ message: 'Restaurant not found' });
            }
            if (restaurant.owner_id.toString() !== id) {
                console.log(restaurant.owner_id);
                console.log(id);
                return res.status(403).json({ message: 'You are not authorized to update this restaurant' });
            }
            next();
        }
        catch (err) {
            console.error(err);
            return res.status(500).json({ message: 'Internal server error' });
        }
    });
};
exports.checkRestaurantOwner = checkRestaurantOwner;
class ExcludedPath {
    constructor(path, methods) {
        this.path = path;
        this.methods = methods;
    }
}
exports.ExcludedPath = ExcludedPath;
