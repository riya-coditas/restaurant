"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Roles = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
exports.Roles = {
    admin: new mongoose_1.default.mongo.ObjectId('64142d25c9de184b14f63470'),
    owner: new mongoose_1.default.mongo.ObjectId('64142d54c9de184b14f63472'),
    customer: new mongoose_1.default.mongo.ObjectId('64142d58c9de184b14f63474')
};
// export const Roles = {
//     admin: "admin",
//     owner: "owner",
//     customer: "customer"
// }
