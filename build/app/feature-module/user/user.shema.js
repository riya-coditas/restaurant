"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModel = exports.userSchema = void 0;
const base_schema_1 = require("../../utils/base-schema");
const mongoose_1 = require("mongoose");
exports.userSchema = new base_schema_1.BaseSchema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }
});
exports.UserModel = (0, mongoose_1.model)("User1", exports.userSchema);
