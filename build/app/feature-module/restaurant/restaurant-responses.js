"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Restaurant_response = void 0;
exports.Restaurant_response = {
    Restaurant_not_found: {
        statusCode: 404,
        message: "Restaurant not found"
    }
};
