import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";
import restaurantServices from "../feature-module/restaurant/restaurant.services";

export const authorize = (excludedPaths:ExcludedPaths) =>{
    return (req:Request,res:Response,next:NextFunction)=>{
        try{
            // if(excludedPaths.find(e=>e.path===req.url && e.methods===req.method)){return next()}
            const token = req.headers.authorization?.split(" ")[1];
            if(!token) return next({message:"UNAUTHORIZED",statusCode:401});
            const {JWT_SECRET} = process.env;
            const result = verify(token,JWT_SECRET||"");
            console.log(result)
            res.locals.tokenInfo = result;
            next();
        }
        catch(e){
            next({message:"UNAUTHORIZED",statusCode:401})
        }
    }
}

export const validateRole = (roles:string[]) =>{
    return (req:Request,res:Response,next:NextFunction)=>{
       try{
                  const { role } = res.locals.tokenInfo;
              
                  if (roles.includes(role)) {
                    next();
                  } else {
                    next({ message: "UNAUTHORIZED", statusCode: 401 });
                  }
       }
        catch(e){
            next({message:"Something went wrong",statusCode:401})
        }
    }
}


export const checkRestaurantOwner = () => {
 return async (req:Request,res:Response,next:NextFunction)=> {
  try {
    const { id } = res.locals.tokenInfo;

   const restaurantId = req.params.id
   console.log(restaurantId)

   const restaurant = await restaurantServices.findOne({ _id : restaurantId });

   if (!restaurant) {
     return res.status(404).json({ message: 'Restaurant not found' });
   }

   if (restaurant.owner_id.toString() !== id) {
     console.log(restaurant.owner_id)
     console.log(id)
     return res.status(403).json({ message: 'You are not authorized to update this restaurant' });
   }
  

    next();
  } catch (err) {
    console.error(err);
    return res.status(500).json({ message: 'Internal server error' });
  }
 }
}


type methods = "GET" | "POST" | "PUT" | "PATCH" | "DELETE";
export class ExcludedPath{
    constructor(
        public path : string,
        public methods :methods
    ){}
}
export type ExcludedPaths = ExcludedPath[];