import {Route, Routes} from "./routes.type"
import  authRouter  from "../feature-module/auth/auth.routes"
import userRouter from "../feature-module/user/user.routes"
import roleRouter from '../feature-module/role/role.routes'
import restaurantRouter from '../feature-module/restaurant/restaurant-routes'
import { ExcludedPath, ExcludedPaths } from "../utils/authorize"

export const routes : Route = [

    new Routes('/auth', authRouter),
    new Routes('/users', userRouter),
    new Routes('/role', roleRouter),
    new Routes('/restaurant',restaurantRouter)
   
] 

export const excludedPaths: ExcludedPaths = [
    new ExcludedPath("/auth/login", "POST"),
    new ExcludedPath("/auth/register", "POST")
];