import { Router,Request,Response,NextFunction } from "express";
import { excludedPaths } from "../../routes/routes.data";
import { authorize, checkRestaurantOwner, validateRole } from "../../utils/authorize";
import { ResponseHandler } from "../../utils/response_handler";
import restaurantServices from "./restaurant.services";

const router = Router();

router.post('/RegisterRestaurant',authorize(excludedPaths),validateRole(['64142d58c9de184b14f63474']),async (req:Request,res:Response,next:NextFunction)=>{
    try{
        const restaurantData = req.body;
        const result = await restaurantServices.RegisterRestaurant(restaurantData);
        res.send(new ResponseHandler(result))
    }
    catch(e){
        next(e)
    }
})

router.post('/ApproveRestaurant/:id',authorize(excludedPaths),validateRole(["64142d25c9de184b14f63470"]),async (req:Request,res:Response,next:NextFunction)=>{
    try{
        const restaurantId = req.params.id;
        const restaurantCredentials = { _id: restaurantId };
        const result = await restaurantServices.ApproveRestaurant(restaurantCredentials);
        res.send(new ResponseHandler(result))
    }
    catch(e){
        next(e)
    }
})

router.post('/RejectRestaurant/:id',authorize(excludedPaths),validateRole(["64142d25c9de184b14f63470"]),async (req:Request,res:Response,next:NextFunction)=>{
    try{
        const restaurantId = req.params.id;
        const restaurantCredentials = { _id: restaurantId };
        const result = await restaurantServices.RejectRestaurant(restaurantCredentials);
        res.send(new ResponseHandler(result))
    }
    catch(e){
        next(e)
    }
})


router.post('/UpdateRestaurant/:id',authorize(excludedPaths),validateRole(["64142d54c9de184b14f63472"]),checkRestaurantOwner(),async (req:Request,res:Response,next:NextFunction)=>{
    try{
        const id = req.params.id;
        const update = req.body;

        const result = await restaurantServices.updateMyRestaurant(id,update);
        res.send(new ResponseHandler(result))
    }
    catch(e){
        next(e)
    }
})

export default router