import { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import { RestaurantModel } from "./restaurant-schema";
import { IRestaurant } from "./restaurant.types";

const create = (restaurant:IRestaurant) => RestaurantModel.create(restaurant);

const findByIdAndUpdate = (filter: FilterQuery<IRestaurant>, update: UpdateQuery<IRestaurant>, options: QueryOptions = {}) => {
    return RestaurantModel.findOneAndUpdate(filter, update, { ...options, new: true });
  };
  

const find = async()=>{
    try{
        return await RestaurantModel.find({
            isDeleted:false,
            isApproved:true
        })
    }
    catch(err){
        throw {message:"something went wrong, please try again",e:err}
    }
}

const findOne = async (filters:Partial<IRestaurant>) => {
    try {
        return await RestaurantModel.findOne({
            ...filters,
            isDeleted:false,
            isApproved:true,
            isRejected:false
        })
    } catch (err) {
        throw { message: 'something went wrong', e: err } 
    } 
}

export default {
    create,
    findByIdAndUpdate,
    find,
    findOne
}
