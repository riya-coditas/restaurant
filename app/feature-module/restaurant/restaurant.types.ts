import mongoose, { ObjectId } from "mongoose"

export interface IRestaurant {
    _id?: string,
    restaurant_name:string,
    owner_id:ObjectId,
    restaurant_location:string,
    branch_names:string[],
    category:string,
    menu? : IFoodItem[];
}

export interface IFoodItem{
    itemName: string;
    itemPrice: string;
}


export interface IRestaurantCredentials{
    _id:string
}