import { FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import restaurantRepo from "./restaurant.repo";
import { IRestaurant, IRestaurantCredentials} from "./restaurant.types";
import { Restaurant_response } from "./restaurant-responses";
import userServices from "../user/user.services";
import { Roles } from "../role/role.types";

const create = (restaurant:IRestaurant)=>restaurantRepo.create(restaurant);

const findByIdAndUpdate = (filter:FilterQuery<IRestaurant>,update:UpdateQuery<IRestaurant>,options: QueryOptions = {}) =>{
    return restaurantRepo.findByIdAndUpdate(filter,update)
}

const find = async () => {
    const list = await restaurantRepo.find();
    if(!list) throw Restaurant_response.Restaurant_not_found

    return list;
}

const findOne = async (filters:Partial<IRestaurant>) => {
    const restaurant = await restaurantRepo.findOne(filters);
    if(!restaurant) throw Restaurant_response.Restaurant_not_found
    
    return restaurant;
}

const RegisterRestaurant = async(data:IRestaurant) =>{
    const restaurant = await restaurantRepo.create(data);
    
    const userId = data.owner_id; 
    const updatedUser = await userServices.findByIdAndUpdate(userId, { role: Roles.owner });

    console.log(restaurant);
    return restaurant;
}

const ApproveRestaurant = async(id:IRestaurantCredentials)=>{
   const filter = {_id : id._id};
   const update = {isApproved:true , isRejected:false}
   const restaurant = await restaurantRepo.findByIdAndUpdate(filter,update);
   console.log(filter,update)
   return restaurant;
}


const RejectRestaurant = async(id:IRestaurantCredentials)=>{
   const filter = {_id : id._id};
   const update = {isApproved: false, isRejected: true};
   const restaurant = await restaurantRepo.findByIdAndUpdate(filter,update);
   console.log(filter,update);
   return restaurant;
}



const updateMyRestaurant = async(restaurantId : string, update : any) => {
   const updatedtResponse = await restaurantRepo.findByIdAndUpdate({ _id : restaurantId},update)
   return updatedtResponse
}



export default {
    create,
    findByIdAndUpdate,
    find,
    findOne,
    RegisterRestaurant,
    ApproveRestaurant,
    RejectRestaurant,
    updateMyRestaurant
}