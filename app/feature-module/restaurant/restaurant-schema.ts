import { Schema, model } from "mongoose";
import { BaseSchema } from "../../utils/base-schema";
import { IRestaurant } from "./restaurant.types";

const RestaurantSchema = new BaseSchema({
    restaurant_name: {
        type:String,
        required:true
    },
    owner_id: {
        type:Schema.Types.ObjectId,
        ref: "users",
        required:true
    },
    restaurant_location: {
        type:String,
        required:true
    },
    branch_names: {
        type:[String],
        required:true
    },
    category: {
        type:String,
        required: true
    },
    menu: {
        type: [
            {
                itemName: String,
                itemPrice: String
            }
        ],
        required: true
    }
}
)

type RestaurantDocument = Document & IRestaurant;
export const RestaurantModel = model<RestaurantDocument>("Restaurants",RestaurantSchema);