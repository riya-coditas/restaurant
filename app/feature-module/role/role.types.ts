import mongoose from "mongoose"

export interface IRole {
    _id: string,
    name: string
}

export const Roles = {
    admin: new mongoose.mongo.ObjectId('64142d25c9de184b14f63470'),
    owner: new mongoose.mongo.ObjectId('64142d54c9de184b14f63472'),
    customer: new mongoose.mongo.ObjectId('64142d58c9de184b14f63474')
}


// export const Roles = {
//     admin: "admin",
//     owner: "owner",
//     customer: "customer"
// }