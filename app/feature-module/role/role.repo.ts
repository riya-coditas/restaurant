import { RoleModel } from "./role.schema";
import { IRole } from "./role.types";


const create = (role : IRole) => RoleModel.create(role);


export default{
    create,

}