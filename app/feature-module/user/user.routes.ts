import { Router,Request,Response,NextFunction } from "express";
import userService from "./user.services";
import { ResponseHandler } from "../../utils/response_handler";


const router = Router();


router.get('/',async (req,res,next)=>{
    try{
        const result = await userService.findAllRestaurants()
        res.send(new ResponseHandler(result))
    }
    catch(e){
        next(e)
    }
})

export default router


